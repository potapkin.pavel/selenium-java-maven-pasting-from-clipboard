package ru.samara;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import static java.util.concurrent.TimeUnit.SECONDS;

public class TestLocalDriver {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setUp() {
        driver = new ChromeDriver(new ChromeOptions());
        driver.manage().timeouts().implicitlyWait(5, SECONDS);
        wait = new WebDriverWait(driver, 5);
        driver.navigate().to("https://www.google.com/");

    }

    @Test
    public void test_pasteFromClipboard() {
        String myString = "Selenium";
        StringSelection stringSelection = new StringSelection(myString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);

        WebElement input = driver.findElement(By.cssSelector("[name='q']"));

        Actions builder = new Actions(driver);
        builder.keyDown(input, Keys.CONTROL).perform();
        builder.sendKeys(input, "v").perform();
        builder.keyUp(input, Keys.CONTROL).perform();

        driver.findElement(By.cssSelector("[name='btnK']")).submit();

        wait.until(ExpectedConditions.refreshed(ExpectedConditions.titleContains("Selenium")));

        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getTitle());
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
