package ru.samara;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.concurrent.TimeUnit.SECONDS;

public class TestRemoteDriver {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), new ChromeOptions());
        driver.manage().timeouts().implicitlyWait(5, SECONDS);
        wait = new WebDriverWait(driver, 5);
        driver.navigate().to("https://www.google.com/");

    }

    @Test
    public void test_pasteFromClipboard() {
        // String myString = "Selenium";
        // StringSelection stringSelection = new StringSelection(myString);
        // Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // clipboard.setContents(stringSelection, null);

        WebElement input = driver.findElement(By.cssSelector("[name='q']"));
        input.sendKeys("Selenium");

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript(
            "let copyText = document.querySelector(\"[name='q']\");\n" +
            "copyText.select();\n" +
            "document.execCommand(\"copy\");");

        input.clear();

        Actions builder = new Actions(driver);
        builder.keyDown(input, Keys.CONTROL).perform();
        builder.sendKeys(input, "v").perform();
        builder.keyUp(input, Keys.CONTROL).perform();

        driver.findElement(By.cssSelector("[name='btnK']")).submit();

        wait.until(ExpectedConditions.refreshed(ExpectedConditions.titleContains("Selenium")));

        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getTitle());
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
